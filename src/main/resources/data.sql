insert into exchange_value(id, currency_from, currency_to, conversion_multiple)
values(1001,'USD','AUD',75);
insert into exchange_value(id, currency_from, currency_to, conversion_multiple)
values(1002,'USD','EUR',55);
insert into exchange_value(id, currency_from, currency_to, conversion_multiple)
values(1003,'GBP','USD',25);